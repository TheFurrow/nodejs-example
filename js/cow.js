"use strict";

class Cow {
	
	constructor(name) {
		Object.assign(this, {
			_name:name,
			_getName() {return this._name}
			
		});
		
		console.log("Lenient cow appears from the woods...");
	}

	moo() {
		return "Moo!";
	}

	eat() {
		return this._getName() + " chews some juicy grass.";
	}

	poop() {
		return this._getName() + " took dump on a sunset.";
	}
}


if(typeof module !== 'undefined' && typeof module.exports !== 'undefined') { module.exports = Cow; }
else { window.Cow = Cow; }

