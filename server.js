var express = require('express');
var app = express();

//Our superior cow!
var Cow = require('./js/cow.js');


//Server
app.use('/', function(req,res) {
	var cow = new Cow("Mimmi the Cow");
	var msg = cow.moo() +" "+ cow.eat();
	res.send(msg);
});

app.listen('3000', function() {
	console.log('Running!');
});
